<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" async href="../assets/css/admin.css">
    <title>Anketa</title>
</head>
<body>
    
    <form method="POST" id="anketForm" action="">
        <div class="tab">
            <?php include (dirname(__FILE__).'/../admin/connect.php'); ?>
        </div>
        <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
        </div>
    </form>
    <script type="text/javascript" src="../assets/js/main.js"></script>
    
</body>
</html>