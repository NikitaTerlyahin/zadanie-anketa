<?php
$monthOptions = '<option value="0" id="month_option">Месяц:</option>';
$dayOptions = '<option value="0" id="day_option">День:</option>';
$yearOptions = '<option value="0" id="year_option">Год:</option>';
 
for($month=1; $month<=12; $month++)
{
   $monthName = date("M", mktime(0, 0, 0, $month));
   $monthOptions .= "<option value=\"{$month}\">{$monthName}</option>\n";
}
for($day=1; $day<=31; $day++)
{
   $dayOptions .= "<option value=\"{$day}\">{$day}</option>\n";
}
for($year=2021; $year>=1890; $year--)
{
   $yearOptions .= "<option value=\"{$year}\">{$year}</option>\n";
}
?>

 
<script type="text/javascript" src="/assets/js/bday.js"></script>
<P>Дата рождения:<br>
<select name="day" id="day">
<?php echo $dayOptions; ?>
</select>
 
<select name="month" id="month" onchange="updateDays();">
<?php echo $monthOptions; ?>
</select>
 
<select name="year" id="year" onchange="updateDays();">
<?php echo $yearOptions; ?>
</select>