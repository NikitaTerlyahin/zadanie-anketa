<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" async href="../assets/css/style.css">
    <title>Anketa</title>
</head>
<body>
    
    <form method="POST" id="anketForm" action="../public/connect.php">
        <h1>Anketa:</h1>
        <div class="tab">Ваше имя:
            <p><input placeholder="Ваше имя*..." oninput="this.className = ''" name="fname"></p>
            <p><input placeholder="Ваша фамилия*..." oninput="this.className = ''" name="lname"></p>
            <p><input placeholder="Ваше отечество*..." oninput="this.className=''" name="mname"></p>
            <?php include (dirname(__FILE__).'/../public/bday.php'); ?>           
            <p><input type="radio" name="gender" value="female"  oninput="this.className = ''" style=
            "width: 2%;"
            >Female</p>
            <p><input type="radio" name="gender" value="male"  oninput="this.className = ''" style=
            "width: 2%;"
            >Male</p>
            <p>*-обязательные поля</p>
            <script type="text/javascript" src="../assets/js/bday.js"></script>
        </div>

        <div class="tab">Загрузите аватар* и выберите цвет: 
            <p><input type="file" name="avatar[]" oninput="this.className =''">не более 100кб, *png, *jpeg/*jpg</p>
            <p>Выберите свой любимый цвет: <input name="color_picker" type="color" oninput="this.className =''" style="
            padding: 0;
            width: 20%;
            "/></p>
            <p>*-обязательные поля</p>
        </div>

        <div class="tab">Личные качества:
            <textarea rows="10" cols="50" name="compdescription" form="anketForm" placeholder="Напишите свои личные качества:"></textarea>
            <p><input type="checkbox" name="checkone" value="Усидчивость" oninput="this.className =''" style=
            "width: 2%;"
            >Усидчивость</p>
            <p><input type="checkbox" name="checktwo" value="Опрятность" oninput="this.className =''" style=
            "width: 2%;"
            >Опрятность</p>
            <p><input type="checkbox" name="checkthree" value="Самообучаемость" oninput="this.className =''" style=
            "width: 2%;"
            >Самообучаемость</p>
            <p><input type="checkbox" name="checkfour" value="Трудолюбие" oninput="this.className =''" style=
            "width: 2%;"
            >Трудолюбие</p>
        </div>

        <div class="tab">Вы можете загрузить до 5 изображени:
            <input type="file" name="images_one[]" oninput="this.className =''" multiple>
            <input type="file" name="images_two[]" oninput="this.className =''" multiple>
            <input type="file" name="images_three[]" oninput="this.className =''" multiple>
            <input type="file" name="images_four[]" oninput="this.className =''" multiple>
            <input type="file" name="images_five[]" oninput="this.className =''" multiple>
        </div>



        <div style="overflow: auto;">
            <div style="float:right";>
                <button type="button" id="prevBtn" onclick="nextPrev(-1)">Назад</button>
                <button type="button" id="nextBtn" onclick="nextPrev(1)" name="">Вперёд</button>
            </div>
        </div>

        <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
        </div>
    
    </form>
    <script type="text/javascript" src="../assets/js/main.js"></script>
    
</body>
</html>