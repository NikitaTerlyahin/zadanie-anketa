<?php
if (isset($_FILES['images']))
    foreach($_FILES['images'] as $key => $value) {
        foreach($value as $k => $v) {
            $_FILES['images'][$k][$key] = $v;
        
        unset($_FILES['images'][$key]);

    foreach ($_FILES['images'] as $k => $v) 
        $fileName = $_FILES['images'][$k]['name'];
        $image_one = $_FILES['images'][$k]['tmp_name'];
        $fileType = $_FILES['images'][$k]['type'];
        $fileSize = $_FILES['images'][$k]['size'];
        $errorCode = $_FILES['images'][$k]['error'];
        if ($errorCode !== UPLOAD_ERR_OK || !is_uploaded_file($image_one)) {
            $errorMessages = [
                UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
                UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
                UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
                UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
                UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
                UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
                UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
            ];
            $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';
            $outputMessage = isset($errorMessages[$errorCode]) ? $errorMessages[$errorCode] : $unknownMessage;
            die($outputMessage);
        } else {
            $fi = finfo_open(FILEINFO_MIME_TYPE);
            $mime = (string) finfo_file($fi, $image_one);
            if (strpos($mime, 'image') === false) die('Можно загружать только изображения.');
            $image = getimagesize($image_one);
            $limitBytes  = 1024 * 1024 * 5;
            $limitWidth  = 1280;
            $limitHeight = 768;
            if (filesize($image_one) > $limitBytes) die('Размер изображения не должен превышать 5 Мбайт.');
            if ($image[1] > $limitHeight)             die('Высота изображения не должна превышать 768 точек.');
            if ($image[0] > $limitWidth)              die('Ширина изображения не должна превышать 1280 точек.');
            $name = getRandomFileName($image_one);
            $extension = image_type_to_extension($image[2]);
            $format = str_replace('jpeg', 'jpg', $extension);
            $image_one = $_FILES['image']['tmp_name'];
            if (isset($_POST['image_one[]'])) {
                $image_one = addslashes(file_get_contents($_FILES['image_one']['tmp_name']));
                $image_one_name = addslashes($_FILES['image_one']['name']);
                if (!mysqli_query($link, "INSERT INTO `image` (`image_one`, `image_one_name`) VALUES ('{$image_one}', '{$image_one_name}')")) { // Error handling
                    echo "Image added";
                } else {
                    echo "Error";
                }
            } else {
                $image_one = "Вы не загрузили изображение";
            }
        }
    };
    echo 'Файлы успешно загружены!';
};



function getRandomFileName($path)
{
    $path = $path ? $path . '/' : '';

    do {
        $name = md5(microtime() . rand(0, 9999));
        $file = $path . $name;
    } while (file_exists($file));

    return $name;
}

?>